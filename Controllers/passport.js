// Example user database
const users = [
    { id: 1, username: 'admin', password: 'password' }
  ];
  
  // Serialize and deserialize user for session management
  passport.serializeUser((user, done) => {
    done(null, user.id);
  });
  
  passport.deserializeUser((id, done) => {
    const user = users.find(u => u.id === id);
    done(null, user);
  });
  
  // Configure local authentication strategy
  passport.use(new LocalStrategy((username, password, done) => {
    const user = users.find(u => u.username === username);
  
    if (!user || user.password !== password) {
      return done(null, false);
    }
  
    return done(null, user);
  }));
  