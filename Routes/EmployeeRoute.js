// routes.js

const express = require('express');
const router = express.Router();
const employeeController = require('../Controllers/EmployeeController');

// Route for employee registration
router.post('/register', employeeController.registerEmployee);

router.get('/',employeeController.getAllEmployee)


router.get('/pagination',employeeController.paginationEmployee)

router.get('/sorting',employeeController.sortingEmployee)

router.get('/search',employeeController.searchEmployee)


router.delete('/:id',employeeController.deleteEmployee)

router.put('/:id',employeeController.updateEmployee)

module.exports = router;
