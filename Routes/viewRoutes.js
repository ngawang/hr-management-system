const viewController = require('../Controllers/viewController')
const express = require('express');
const router = express.Router();

// router.get('/',viewController.getmain)
router.get('/home',viewController.getHome)
router.get('/',viewController.getLogin)
router.get('/register',viewController.getForm)

router.get('/result',viewController.getResult)
router.get('/update',viewController.updateEmployee)



module.exports = router;

