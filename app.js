const express = require("express")
const app = express()
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const path = require('path')
const bodyParser = require('body-parser');
const userRoutes = require('./Routes/userRoutes')
const EmployeeRoute = require('./Routes/EmployeeRoute')
const viewroutes = require('./Routes/viewRoutes')

require('dotenv').config();
app.use(express.static(path.join(__dirname, 'views')))
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
    secret: 'your-secret-key',
    resave: false,
    saveUninitialized: false
  }));
// routes
app.use('/api/v1/users',userRoutes)

app.use('/api/v1/employee',EmployeeRoute)

app.use('/',viewroutes)





  // Initialize passport middleware
app.use(passport.initialize());
app.use(passport.session());

module.exports = app
