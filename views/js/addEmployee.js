const register = async (name, employeeId, gender, dob, designation, department, appointmentDate) => {
    try {
      const response = await axios.post('http://localhost:4001/api/v1/employee/register', {
        name,
        employeeId,
        gender,
        dob,
        designation,
        department,
        appointmentDate,
      });
      
      if (response.status === 200) {
        alert('Success', 'Employee registered successfully');
        window.setTimeout(() => {
          location.assign('/home');
        }, 1500);
      }
    } catch (error) {
      let message = typeof error.response !== 'undefined' ? error.response.data.message : error.message;
      alert('Error', 'Unable to register employee', message);
    }
  };
  
  document.querySelector('.form').addEventListener('submit', (e) => {
    e.preventDefault();
    
    const name = document.getElementById('name').value;
    const employeeId = document.getElementById('employeeId').value;
    const gender = document.getElementById('gender').value;
    const dob = document.getElementById('dob').value;
    const designation = document.getElementById('designation').value;
    const department = document.getElementById('department').value;
    const appointmentDate = document.getElementById('appointmentDate').value;
    
    register(name, employeeId, gender, dob, designation, department, appointmentDate);
  });
  