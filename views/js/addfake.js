const reservation = async(enrollment, date, time, game_type) => {
    try {
        const res = await axios({
            method: 'POST',
            url: 'http://localhost:4001/api/v1/employee/register',
            data: {
                enrollment,
                date,
                time,
                game_type,

            },
        })
        if (res.data.status === 'success') {
            showAlert('success', 'You reserved the ground')
            window.setTimeout(() => {
                location.assign('/reservationpage')
            }, 1500)
        }

    } catch (err) {
        let message = typeof err.response !== 'undefined' ?
            err.response.data.message :
            err.message
        showAlert('error', 'unable to book the ground', message)
    }
}
document.querySelector('.form').addEventListener('submit', (e) => {
    e.preventDefault()
    e.preventDefault()
    const enrollment = document.getElementById('enrollment').value
    const date = document.getElementById('date').value
    const time = document.getElementById('time').value
    const game_type = document.getElementById('game_type').value
    reservation(enrollment, date, time, game_type)

})