const pageSize = 5; // Number of items per page
let currentPage = 1; // Current page number

const getEmployees = async (page) => {
  try {
    const response = await fetch(`http://localhost:4001/api/v1/employee?page=${page}&limit=${pageSize}`);
    const data = await response.json();

    // Update the frontend with the retrieved data
    displayService(data);

    console.log(data);
  } catch (error) {
    console.log(error);
  }
};
const displayService = (employees) => {
	var arr = employees.data;
	// console.log("Hihdjb")
	console.log(arr.length);
	const tableBody = document.querySelector('table')
	for (let i = 0; i < arr.length; i++) {
		const element = arr[i];
		const row = document.createElement('tr')
		row.classList.add('data');
		row.id = element._id
	
		const cell1 = document.createElement('td')
		cell1.textContent = `${element.name}`
		row.appendChild(cell1)
	
		const cell2 = document.createElement('td')
		cell2.textContent = `${element.employeeId}`
		row.appendChild(cell2)
	
		const cell3 = document.createElement('td')
		cell3.textContent = `${element.gender}`
		row.appendChild(cell3)
	
		const cell4 = document.createElement('td')
		cell4.textContent = `${element.dob}`
		row.appendChild(cell4)
	
	
		const cell5 = document.createElement('td')
		cell5.textContent = `${element.designation}`
		row.appendChild(cell5)
		
		const cell6 = document.createElement('td')
		cell6.textContent = `${element.department}`
		row.appendChild(cell6)
		
		const cell7 = document.createElement('td')
		cell7.textContent = `${element.appointmentDate}`
		row.appendChild(cell7)
		
		const updateCell = document.createElement('td');
		const updateButton = document.createElement('button');
		updateButton.innerText = 'Edit';
		updateButton.classList.add('update-button');
		updateCell.appendChild(updateButton);
		row.appendChild(updateCell);
	
		const deleteCell = document.createElement('td');
		const deleteButton = document.createElement('button');
		deleteButton.innerText = 'Delete';
		deleteButton.classList.add('delete-button');
		deleteCell.appendChild(deleteButton);
		row.appendChild(deleteCell);
	
		
		tableBody.appendChild(row);
	//   console.log(element)
	}
};

// Initial page load
getEmployees(currentPage);

// Example: Implement pagination buttons or logic to navigate through pages
const prevPageButton = document.getElementById('prev-page');
const nextPageButton = document.getElementById('next-page');

prevPageButton.addEventListener('click', () => {
  if (currentPage > 1) {
    currentPage--;
    getEmployees(currentPage);
  }
});

nextPageButton.addEventListener('click', () => {
  currentPage++;
  getEmployees(currentPage);
});

//   allService();
  

document.addEventListener('click', async (event) => {
if (event.target.classList.contains('delete-button')) {
	// Delete functionality
	var card = event.target.parentNode.parentNode;
	var rowIndex = Array.from(card.parentNode.children).indexOf(card);
	console.log(rowIndex);

	try {
	const res = await axios.delete(`http://localhost:4001/api/v1/employee/${card.id}`);
	console.log(res); 
	card.remove();
	} catch (err) {
	console.log(err);
	}
} 
});

document.addEventListener('click', (event) => {
if (event.target.classList.contains('update-button')) {
	const card = event.target.parentNode.parentNode;
	const employeeId = card.id;

	// Redirect to the update form page with the employee ID
	window.location.href = `update-form.html?id=${employeeId}`;
}
});
  

//   document.addEventListener('DOMContentLoaded', () => {
// 	const sortButton = document.getElementById('sort-button');
// 	sortButton.addEventListener('click', async () => {
// 	  try {
// 		const res = await axios({
// 		  method: 'GET',
// 		  url: 'http://localhost:4001/api/v1/employee/sorting',
// 		});
// 		if (res.data.status === 'success') {
// 		  const sortedData = res.data.data;
  
// 		  // Convert the sorted data to a query parameter string
// 		  const queryParams = new URLSearchParams({ sortedData: JSON.stringify(sortedData) });
  
// 		  // Redirect to the new route with the query parameter
// 		  window.location.assign('/result');
		  
// 		}
// 	  } catch (err) {
// 		console.log(err);
// 	  }
// 	});
//   });
document.addEventListener('click', async (event) => {
	if (event.target.classList.contains('logout')) {
		// Delete functionality
		var card = event.target.parentNode.parentNode;
		var rowIndex = Array.from(card.parentNode.children).indexOf(card);
		console.log(rowIndex);
	
		try {
			const res = await axios.get(`http://localhost:4001/api/v1/users/logout`);
			console.log(res); 
			if (res.data.status==='success'){
				window.setTimeout(()=>{
					location.assign('/')
				},1500)
			}
		} catch (err) {
			console.log(err);
		}
	} 
}); 


const searchInput = document.getElementById('search-input');
const searchButton = document.getElementById('search-button');

// Attach an event listener to the form submission
document.querySelector('.search-form').addEventListener('submit', async (event)=> {
  event.preventDefault(); // Prevent the form from being submitted and page reload
  
  const searchQuery = searchInput.value; // Get the value entered in the search input
  try {
    const res = await axios.get(`http://localhost:4001/api/v1/employee/search?q=${searchQuery}`)
    
    const searchData = res.data.data;
    const encodedData = encodeURIComponent(JSON.stringify(searchData));
    
    // Redirect to result.html with the search query as a query parameter
    window.location.href = `result.html?query=${encodeURIComponent(searchQuery)}&data=${encodedData}`;
  } catch (error) {
    console.log(error)
    
  }
  
});
